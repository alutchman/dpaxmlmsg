package com.dpa.controller;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Ignore
public class TestHomeController {
    public static final String urlbase= "http://localhost:8080/DpaMsgCue/";
    @Test
    public void testxml(){
        final String uri = urlbase+"showxml";
        RestTemplate restTemplate = new RestTemplate();


        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<String> response =
                restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        String resource = response.getBody();
        System.out.println(resource);
    }

    private ResponseEntity<String> postToRestTemplate(String uri, String request, MediaType mediaType){
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> entity = null;
        if (mediaType != null) {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(mediaType);
            entity = new HttpEntity<String>(request, headers);
        } else {
            entity = new HttpEntity<String>(request);
        }
        ResponseEntity<String> result = restTemplate.postForEntity(uri, entity, String.class);
        return result;
    }


    private ResponseEntity<String> getFromRestTemplate(String uri, String request, MediaType mediaType){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);
        HttpEntity<String> entity = new HttpEntity<String>(request, headers);



        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
        return result;
    }


    @Test
    public void testxmlPost() {
        final String uri = urlbase+"handle.xml";
        String request = "<DpaItem  display=\"32\" value=\"3\" mobile=\"4 dagen per week\">Een hele dag vrij per week of 32 uren verdeeld over de hele week.</DpaItem>";

        ResponseEntity<String> result = postToRestTemplate(uri, request, MediaType.APPLICATION_XML);
        System.out.println(result.getBody());
    }


    @Test
    public void getSaldo() {
        final String uri = urlbase+"saldoRequest";
        String request = "<AccountInfoRequest >" +
                "<rekening>499658285</rekening>" +
                "<naam>Amrit Lutchman</naam>" +
                "</AccountInfoRequest>";

        ResponseEntity<String> result = postToRestTemplate(uri, request, MediaType.APPLICATION_XML);
        System.out.println(result.getBody());
    }




    @Test
    public void getSaldoN0ame() {
        final String uri = urlbase+"saldoRequest";
        String request = "<AccountInfoRequest >" +
                "<rekening>499658285</rekening>" +
                "</AccountInfoRequest>";

        ResponseEntity<String> result = postToRestTemplate(uri, request, MediaType.APPLICATION_XML);
        System.out.println(result.getBody());
    }



    @Test
    public void getSaldoWrong() {
        final String uri = urlbase+"saldoRequest";
        String request = "<AccountInfoRequest >" +
                "<rekening>499658285</rekening>" +
                "<naam>Amrit Lutchman</naam>" +
                "";

        ResponseEntity<String> result = postToRestTemplate(uri, request, MediaType.APPLICATION_XML);
        System.out.println(result.getBody());
    }



    //saldoRequest
}
