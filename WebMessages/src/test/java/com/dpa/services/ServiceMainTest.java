package com.dpa.services;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;


@RunWith(MockitoJUnitRunner.class)
public class ServiceMainTest {



    @InjectMocks
    public ServiceMain serviceMain;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testGetDate() throws Exception {
        String currentDate = serviceMain.getDate();
        assertNotNull(currentDate);
        System.out.println(currentDate);
    }



}