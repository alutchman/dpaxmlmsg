package com.dpa.services;

import com.dpa.data.AccountInfoRequest;
import com.dpa.data.AccountInfoResponse;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class SaldoHandlerServiceTest {


    @InjectMocks
    public SaldoHandlerService saldoHandlerService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();



    @Test
    public void testReturnSaldo() throws Exception {
        AccountInfoRequest accountInfoRequest = new AccountInfoRequest();
        accountInfoRequest.setNaam("Unit Test User");
        accountInfoRequest.setRekening("123456789");
        AccountInfoResponse accountInfoResponse = saldoHandlerService.returnSaldo(accountInfoRequest);
        assertNotNull(accountInfoResponse);



    }
}