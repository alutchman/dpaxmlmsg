<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
    </head>
    <body>
        <h1>Hello NS DSO Folks!</h1>
        <p>This is the homepage!</p>

        <p>${date}</p>
        <p>${userName}</p>


        <c:forEach items="${days}" var="day">
            ${day}<br>
        </c:forEach>
    </body>
</html>
