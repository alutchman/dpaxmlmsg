package com.dpa.services;

import com.dpa.data.DpaSelections;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class ServiceMain {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceMain.class);

    @Autowired
    private Jaxb2Marshaller marshaller;

    @Autowired
    private DpaSelections dpaSelections;

    @PostConstruct
    public void  createDpaSelections(){

       LOGGER.info("Found {} options in dpaOptions.xml",dpaSelections.getOptions().size());
    }

    public String getDate() {
        DateTime dt = new DateTime();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss");
        String str = fmt.print(dt);

        return str;
    }

    public DpaSelections getDpaSelections(){
        return dpaSelections;
    }
}
