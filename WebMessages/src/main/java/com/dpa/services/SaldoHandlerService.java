package com.dpa.services;


import com.dpa.ErrorHandling.DpaException;
import com.dpa.data.AccountInfoRequest;
import com.dpa.data.AccountInfoResponse;
import org.springframework.stereotype.Service;

@Service
public class SaldoHandlerService {


    public AccountInfoResponse returnSaldo(AccountInfoRequest accountInfoRequest) {

        if ((accountInfoRequest.getRekening() == null) || (accountInfoRequest.getRekening().trim().length() == 0)) {
            throw new DpaException("Rekening is niet ingevuld");
        }
        if ((accountInfoRequest.getNaam() == null) || (accountInfoRequest.getNaam().trim().length() == 0)) {
            throw new DpaException("Naam is niet ingevuld");
        }
        AccountInfoResponse response = new AccountInfoResponse();
        response.setRekening(accountInfoRequest.getRekening());
        response.setSaldo(100.0f);

        return response;
    }
}
