package com.dpa.controller;

import com.dpa.ErrorHandling.DpaException;
import com.dpa.data.AccountInfoRequest;
import com.dpa.data.AccountInfoResponse;
import com.dpa.data.DpaData;
import com.dpa.data.ErrorResponse;
import com.dpa.services.SaldoHandlerService;
import com.dpa.services.ServiceMain;
import com.dpa.utils.ErrorFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.oxm.MarshallingFailureException;
import org.springframework.oxm.UnmarshallingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {
	private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private ServiceMain serviceMain;

	@Autowired
	private SaldoHandlerService saldoHandlerService;

	@Autowired
	private ErrorFormatter errorFormatter;

	@RequestMapping(value="/")
	public ModelAndView test(){

		List<String> info = new ArrayList<>();
		info.add("Monday");
		info.add("Wednesday");
		info.add("Friday");

		ModelAndView mv = new ModelAndView("home");
		mv.addObject("date", serviceMain.getDate());
		mv.addObject("days", info);

		return mv;
	}


	@RequestMapping(value = "/showxml", method = RequestMethod.GET )
	public @ResponseBody DpaData restXmlTest0(){
		DpaData dpaItem = new DpaData();
		dpaItem.setValue((float) -5.0);
		dpaItem.setDisplay("Reiskosten");
		dpaItem.setContent("Eigen vervoer: Openbaar vervoer of kilometer vergoeding");
		dpaItem.setMobile("km vergoeding/OV");

		LOGGER.info("Requested : /showxml");
		return dpaItem;
	}


	@RequestMapping(value = "/handle.xml", method = RequestMethod.POST , consumes= MediaType.APPLICATION_XML_VALUE)
	public  @ResponseBody  DpaData handleXmlTest0(@RequestBody DpaData dpaItem){
		dpaItem.setContent("........Seen by handleXmlTest0 .............");
		return dpaItem;
	}

	@RequestMapping(value = "/saldoRequest",
			method = RequestMethod.POST ,
			consumes= MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody AccountInfoResponse saldoInfo(@RequestBody AccountInfoRequest accountInfoRequest){
		return saldoHandlerService.returnSaldo(accountInfoRequest);
	}



	@ExceptionHandler(DpaException.class)
	public  @ResponseBody ErrorResponse resolveException(HttpServletRequest req, DpaException exception) {
		LOGGER.error("{} ::: {}", req.getRequestURI(), exception.getMessage());
		return errorFormatter.create(exception.getMessage(), req.getRequestURI());
	}


	@ExceptionHandler({UnmarshallingFailureException.class, MarshallingFailureException.class})
	public @ResponseBody  ErrorResponse resolveSaxException(HttpServletRequest req,RuntimeException exception) {
		LOGGER.error("{} ::: {}", req.getRequestURI(), exception.getMessage());
		return errorFormatter.create(exception.getMessage(), req.getRequestURI());
	}



	@ExceptionHandler({HttpClientErrorException.class})
	public ErrorResponse resolveException(HttpServletRequest req, RuntimeException exception) {
		LOGGER.error("{} ::: {}", req.getRequestURI(), exception.getMessage());
		return errorFormatter.create(exception.getMessage(), req.getRequestURI());
	}


	@RequestMapping(value = {
			"{path:(?!webjars|static).*$}",
			"{path:(?!webjars|static).*$}/**",
			"{path:(?!webjars|static).*$}/**.html"
		},
		headers={"Accept=application/xml", "Accept=text/html"}, produces= MediaType.APPLICATION_XML_VALUE,
		method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE, RequestMethod.TRACE}
	)

	public ErrorResponse pageNotFound(HttpServletRequest req){
		LOGGER.error("Page not Found: {}", req.getRequestURI());

		return errorFormatter.create("Page Not Found", req.getRequestURI());
	}



}
