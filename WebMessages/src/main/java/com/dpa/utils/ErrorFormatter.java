package com.dpa.utils;

import com.dpa.data.ErrorResponse;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.stereotype.Component;

@Component
public class ErrorFormatter {


    public ErrorResponse create(String message, String url){
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setError(message);
        errorResponse.setUrl(url);

        DateTime joda = new DateTime(DateTimeZone.UTC);

        errorResponse.setTime(joda.toDateTimeISO());
        return errorResponse;
    }
}
