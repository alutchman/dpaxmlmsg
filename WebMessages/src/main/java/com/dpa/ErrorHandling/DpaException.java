package com.dpa.ErrorHandling;


public class DpaException extends RuntimeException {

    public DpaException(String message){
        super(message);

    }

    public DpaException(String message, Throwable cause) {
        super(message, cause);

    }



}
