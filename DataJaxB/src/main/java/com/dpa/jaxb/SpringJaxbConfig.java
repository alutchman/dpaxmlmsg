package com.dpa.jaxb;


import com.dpa.data.DpaSelections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.xml.transform.StringSource;

import javax.annotation.PostConstruct;
import javax.xml.bind.Marshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan(
        basePackages = {"com.dpa.jaxb", "com.dpa.other"}
)
public class SpringJaxbConfig implements  ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringJaxbConfig.class);

    private static class ValidationEventHandlerImpl implements ValidationEventHandler {
        private static final Logger LOGGER = LoggerFactory.getLogger(ValidationEventHandlerImpl.class);

        @Override
        public boolean handleEvent(ValidationEvent event) {
            LOGGER.info("**VALIDATION XML******* "+event.getLinkedException().getMessage());
            return false;
        }
    }



    @Bean
    public Jaxb2Marshaller getJaxb2Marshaller() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();

        Map<String,Object> map = new HashMap<String,Object>();
        map.put(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        Resource schema = new ClassPathResource("dpaOptions.xsd");

        jaxb2Marshaller.setSchema(schema);
        jaxb2Marshaller.setContextPaths("com.dpa.data");
        jaxb2Marshaller.setMarshallerProperties(map);

        try {

            jaxb2Marshaller.setValidationEventHandler(new ValidationEventHandlerImpl());
            jaxb2Marshaller.afterPropertiesSet();
            LOGGER.info("********************   jaxb2Marshaller loaded ***************************** ");
            return jaxb2Marshaller;
        } catch (Exception e) {
            LOGGER.info("********************  FAILURE jaxb2Marshaller not Loaded ***************************** ");
            return null;
        }

    }


    @Autowired
    @Bean
    public DpaSelections  createDpaSelections(Jaxb2Marshaller marshaller){
        ClassLoader classLoader = getClass().getClassLoader();

        File file = new File(classLoader.getResource("dpaOptions.xml").getFile());
        if (file.exists()) {
            try {
                String content = new String(Files.readAllBytes(file.toPath()));
               return (DpaSelections)  marshaller.unmarshal(new StringSource(content));

            } catch (IOException e) {
                LOGGER.error("File dpaOptions error:  {}", e.getMessage());
            } catch (Exception e) {
                LOGGER.error("JAXBException dpaOptions error:  {}", e.getMessage());
            }
        }
        return new DpaSelections();
    }



    @Autowired
    @Bean
    public MarshallingHttpMessageConverter createMsgConverter(Jaxb2Marshaller jaxb2Marshaller){
        MarshallingHttpMessageConverter xmlConverter =  new MarshallingHttpMessageConverter(jaxb2Marshaller, jaxb2Marshaller);
        xmlConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML));
        return xmlConverter;
    }




    /**
     * This is the place to call initializations like load proxy options. This is
     * were we land when all beans have been (re)loaded.
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        LOGGER.info("********************   onApplicationEvent ***************************** ");
    }








}

