package com.dpa.utils;


import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

import javax.xml.bind.annotation.adapters.XmlAdapter;


public class JodaTimeAdpater extends XmlAdapter<String, DateTime> {
    private static final DateTimeFormatter PatternFORMAT = new DateTimeFormatterBuilder()
                                                            .appendPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")
                                                            .appendTimeZoneOffset("Z", true, 2, 4)
                                                            .toFormatter();

    @Override
    public DateTime unmarshal(String v) throws Exception {
        DateTime result = PatternFORMAT.parseDateTime(v);
        return result;
    }

    @Override
    public String marshal(DateTime v) throws Exception {
        String result   = PatternFORMAT.print(v);
        return result;
    }


}

