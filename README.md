# README #

This application allows you define JAXB2  classes with xsd. They can then be used as request and response objects in your controller. A message converter will automatically convert the STRING to the internal JAXB object.
